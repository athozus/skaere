// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>

#pragma once

#include <QAbstractListModel>
#include <QtNetwork>
#include <QJsonObject>

class LanguagesModel : public QAbstractListModel {
Q_OBJECT;

private:
    QMap<QString, QStringList> m_languages = {};

    static QString formatList(const QStringList &list);

public:
    LanguagesModel();

    enum Roles {
        SourceRole = Qt::UserRole,
        TargetRole
    };

    int rowCount(const QModelIndex &) const override;

    QHash<int, QByteArray> roleNames() const override;

    QVariant data(const QModelIndex &index, int role) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    Q_INVOKABLE int getSourceIndex(const QString &sourceLang);
    Q_INVOKABLE int getTargetIndex(const QString &sourceLang, const QString &targetLang);

    Q_INVOKABLE QString getSourceAtIndex(const int &sourceIndex);
    Q_INVOKABLE QString getTargetAtIndex(const int &sourceIndex, const int &targetIndex);

    Q_INVOKABLE void syncLanguages();
};
