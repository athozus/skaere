#pragma once

#include <QObject>
#include <QtNetwork>
#include <QJsonObject>

class Backend : public QObject
{
    Q_OBJECT

    public:
        explicit Backend(QObject *parent = nullptr);

    public:
        Q_INVOKABLE QString translate(const QString &sourceLang, const QString &targetLang, const QString &text);
        Q_INVOKABLE QString detect(const QString &text);
};
