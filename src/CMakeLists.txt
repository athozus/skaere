# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>

add_executable(skaere
    main.cpp
    about.cpp
    app.cpp
    backend.cpp
    languagesmodel.cpp
    resources.qrc)

target_link_libraries(skaere
    Qt5::Core
    Qt5::Gui
    Qt5::Qml
    Qt5::Quick
    Qt5::QuickControls2
    Qt5::Svg
    Qt5::Network
    KF5::I18n
    KF5::CoreAddons
    KF5::ConfigCore
    KF5::ConfigGui)

kconfig_add_kcfg_files(skaere GENERATE_MOC skaereconfig.kcfgc)
install(TARGETS skaere ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
