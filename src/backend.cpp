#include "backend.h"

#include <QtNetwork>
#include <QJsonObject>

Backend::Backend(QObject *parent)
    : QObject(parent)
{

}

QString Backend::translate(const QString &sourceLang, const QString &targetLang, const QString &text)
{
    // send POST request
    QUrl url("https://libretranslate.de/translate");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QNetworkAccessManager manager;
    QJsonObject data;
    data.insert("source", sourceLang);
    data.insert("target", targetLang);
    data.insert("q", text);
    QNetworkReply *reply = manager.post(request, QJsonDocument(data).toJson());

    while (!reply->isFinished())
    {
        qApp->processEvents();
    }

    // get JSON reply and close request
    QByteArray response_data = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(response_data);
    QJsonObject object = doc.object();
    reply->deleteLater();

    return object["translatedText"].toString();
}

QString Backend::detect(const QString &text)
{
    // send POST request
    QUrl url("https://libretranslate.de/detect");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QNetworkAccessManager manager;
    QJsonObject data;
    data.insert("q", text);
    QNetworkReply *reply = manager.post(request, QJsonDocument(data).toJson());

    while (!reply->isFinished())
    {
        qApp->processEvents();
    }

    // get JSON reply and close request
    QByteArray response_data = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(response_data);
    QJsonArray array = doc.array();
    reply->deleteLater();

    return array.at(0).toObject()["language"].toString();
}
