// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami

import org.kde.skaere 1.0

Kirigami.ApplicationWindow {
    id: root

    title: i18n("skaere")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    onClosing: Controller.saveWindowGeometry(root)

    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()
    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()

    // This timer allows to batch update the window size change to reduce
    // the io load and also work around the fact that x/y/width/height are
    // changed when loading the page and overwrite the saved geometry from
    // the previous session.
    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: Controller.saveWindowGeometry(root)
    }

    property int counter: 0

    globalDrawer: Kirigami.GlobalDrawer {
        title: i18n("skaere")
        titleIcon: "applications-graphics"
        isMenu: !root.isMobile
        actions: [
            Kirigami.Action {
                text: i18n("About skaere")
                icon.name: "help-about"
                onTriggered: pageStack.layers.push('qrc:About.qml')
            },
            Kirigami.Action {
                text: i18n("Quit")
                icon.name: "application-exit"
                onTriggered: Qt.quit()
            }
        ]
    }

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: page

    LanguagesModel {
        id: languagesModel
    }

    Kirigami.Page {
        id: page

        Layout.fillWidth: true

        title: i18n("Skaere")

        GridLayout {
            id: gridLayout

            columns: page.width < Kirigami.Units.gridUnit*36 ? 1 : 2
            width: page.width-2*Kirigami.Units.gridUnit

            anchors.centerIn: parent

            ColumnLayout {
                RowLayout {
                    Controls.ComboBox {
                        id: sourceLang
                        enabled: !autodetect.checked
                        model: languagesModel
                        textRole: "sourceLang"
                        valueRole: "targetLang"
                    }
                    Controls.Button {
                        id: autodetect
                        text: i18n("Auto-detect language")
                        icon.name: "autocorrection"
                        checkable: true
                    }
                }

                Controls.ScrollView {
                    id: sourceScroll
                    Layout.fillWidth: true
                    Layout.preferredHeight: font.pixelSize*12

                    Controls.TextArea {
                        id: sourceText
                        Layout.fillWidth: true
                        Layout.preferredHeight: font.pixelSize*12
                    }
                }
            }

            ColumnLayout {
                RowLayout {
                    Controls.ComboBox {
                        id: targetLang
                        model: sourceLang.currentValue
                    }
                    Controls.Button {
                        id: swap
                        text: i18n("Swap languages")
                        icon.name: "document-swap"
                        onClicked: {
                            // can't make it one-line because all targets are not available for every source
                            var source = languagesModel.getSourceAtIndex(sourceLang.currentIndex)
                            var target = languagesModel.getTargetAtIndex(sourceLang.currentIndex, targetLang.currentIndex)
                            sourceLang.currentIndex = languagesModel.getTargetIndex(source, target)
                            targetLang.currentIndex = languagesModel.getSourceIndex(source)

                            sourceText.text = targetText.text
                            targetText.text = Backend.translate(target, source, sourceText.text)
                        }
                    }
                    Controls.Button {
                        id: translate
                        text: i18n("Translate")
                        icon.name: "crow-translate-tray"
                        onClicked: {
                            progressBar.indeterminate = true
                            if (autodetect.checked) {
                                sourceLang.currentIndex = languagesModel.getSourceIndex(Backend.detect(sourceText.text))
                            }
                            targetText.text = Backend.translate(
                                languagesModel.getSourceAtIndex(sourceLang.currentIndex),
                                languagesModel.getTargetAtIndex(sourceLang.currentIndex, targetLang.currentIndex),
                                sourceText.text)

                            progressBar.indeterminate = false
                            progressBar.value = 100
                        }
                    }
                }

                Controls.ScrollView {
                    id: targetScroll
                    Layout.fillWidth: true
                    Layout.preferredHeight: font.pixelSize*12

                    Controls.TextArea {
                        id: targetText
                        Layout.fillWidth: true
                        Layout.preferredHeight: font.pixelSize*12
                        enabled: sourceText.length > 0
                        readOnly: true
                    }
                }
            }

            Controls.ProgressBar {
                id: progressBar

                Layout.columnSpan: gridLayout.columns
                Layout.fillWidth: true

                from: 0
                to: 100
                value: 0
                indeterminate: false
            }
        }
    }
}
