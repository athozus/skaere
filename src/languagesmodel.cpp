// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: %{CURRENT_YEAR} %{AUTHOR} <%{EMAIL}>

#include "languagesmodel.h"

#include <QtNetwork>
#include <QJsonObject>

LanguagesModel::LanguagesModel()
{
    syncLanguages();
}

int LanguagesModel::rowCount(const QModelIndex &) const {
    return m_languages.count();
}

QHash<int, QByteArray> LanguagesModel::roleNames() const {
    QHash<int, QByteArray> map = {
            {SourceRole,  "sourceLang"},
            {TargetRole,  "targetLang"}
    };
    return map;
}

QVariant LanguagesModel::data(const QModelIndex &index, int role) const {
    const auto it = m_languages.begin() + index.row();
    switch (role) {
        case SourceRole:
            return QLocale::languageToString(QLocale(it.key()).language());
        case TargetRole: {
            QStringList fList = {};
            for (int i = 0; i < it.value().size(); i++) {
                fList.append(QLocale::languageToString(QLocale(it.value()[i]).language()));
            }
            return fList;
        }
        default:
            return {};
    }
}

bool LanguagesModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (!value.canConvert<QString>() && role != Qt::EditRole) {
        return false;
    }

    auto it = m_languages.begin() + index.row();
    QString targetLangsUnformatted = value.toString();
    QStringList targetLangs = targetLangsUnformatted.split(", ");

    m_languages[it.key()] = targetLangs;
    emit dataChanged(index, index);

    return true;
}

void LanguagesModel::syncLanguages() {
    // send GET request
    QUrl url("https://libretranslate.de/languages");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QNetworkAccessManager manager;
    QNetworkReply *reply = manager.get(request);

    while (!reply->isFinished())
    {
        qApp->processEvents();
    }

    // get JSON reply and close request

    QByteArray response_data = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(response_data);
    QJsonArray array = doc.array();
    reply->deleteLater();

    // store JSON into m_languages
    m_languages.clear();
    foreach (const QJsonValue& value, array) {
        QJsonObject obj = value.toObject();
        QString code = obj["code"].toString();
        QJsonArray targets = obj["targets"].toArray();

        QStringList targetsList;
        foreach (const QJsonValue& target, targets) {
            targetsList.append(target.toString());
        }

        m_languages[code] = targetsList;
    }
}

int LanguagesModel::getSourceIndex(const QString &sourceLang) {
    int i = 0;
    for (auto it = m_languages.begin(); it != m_languages.end(); ++it) {
        if (it.key() == sourceLang) {
            return i;
        }
        i++;
    }
    return -1;
}

int LanguagesModel::getTargetIndex(const QString &sourceLang, const QString &targetLang) {
    return m_languages[sourceLang].indexOf(targetLang);
}

QString LanguagesModel::getSourceAtIndex(const int &sourceIndex) {
    int i = 0;
    for (auto it = m_languages.begin(); it != m_languages.end(); ++it) {
        if (i == sourceIndex) {
            return it.key();
        }
        i++;
    }
    return "";
}

QString LanguagesModel::getTargetAtIndex(const int &sourceIndex, const int &targetIndex) {
    return m_languages[getSourceAtIndex(sourceIndex)][targetIndex];
}
